﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DataService
    {
        private readonly ApplicationContext _dbContext;
        public DataService(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public User GetUser(User user)
        {
            return _dbContext.Users.Where(el => el.Login.Equals(user.Login) && el.Password.Equals(user.Password))
                .Include(el => el.Role_User_Mappings.Select(x => x.Role))
                .FirstOrDefault();
        }

        public User RegisterUser(User user)
        {
            var userInDb = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login));
            var role = _dbContext.Roles.FirstOrDefault(el => el.RoleName.Equals("User"));
            if(userInDb!= null || role == null)
            {
                throw new ArgumentException();
            }
            var query = @"INSERT INTO Users (Login, Password) VALUES (@Login, @Password)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Login", user.Login), new SqlParameter("@Password", user.Password));
            var userId = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login)).Id;
            query = @"INSERT INTO Role_User_Mappings (RoleId, UserId) VALUES (@RoleId, @UserId)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@RoleId", role.Id), new SqlParameter("@UserId", userId));

            return _dbContext.Users.Where(el => el.Login.Equals(user.Login) && el.Password.Equals(user.Password))
               .Include(el => el.Role_User_Mappings.Select(x => x.Role))
               .FirstOrDefault();
        }

    }
}
