﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client
{
    public class SingletoneObj
    {
        public static Dictionary<string, Window> Windows { get; set; } = new Dictionary<string, Window>();
        public static User User { get; set; }
        public static int Port { get; set; } = 8888;
        public static string Ip { get; set; } = "127.0.0.1";
        public static TcpClient Client { get; set; }
        public static NetworkStream Stream { get; set; }
    }
}
