﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private readonly TcpService tcpService;

        public LoginWindow()
        {
            InitializeComponent();
            if (SingletoneObj.Windows.ContainsKey("MainWindow"))
            {
                SingletoneObj.Windows["MainWindow"].Close();
                SingletoneObj.Windows.Remove("MainWindow");
            }
            if (SingletoneObj.Windows.ContainsKey("RegisterWindow"))
            {
                SingletoneObj.Windows["RegisterWindow"].Close();
                SingletoneObj.Windows.Remove("RegisterWindow");
            }
            tcpService = new TcpService();
            
        }

        private async  void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InBtn.IsEnabled = false;
                LoginBtn.IsEnabled = false;
                RegistrationBtn.IsEnabled = false;

                List<string> errors = new List<string>();
                string login = textBoxLogin.Text;
                string password = passBox.Password;
                if (string.IsNullOrWhiteSpace(login))
                    errors.Add($"'{nameof(login)} is incorrect'");
                if (string.IsNullOrWhiteSpace(password))
                    errors.Add($"'{nameof(password)}' is incorrect");

                if (errors.Any())
                {
                    string showErrorsStr = "";
                    foreach (var error in errors)
                        showErrorsStr += error + "\n";
                    showErrorsStr = showErrorsStr.Remove(showErrorsStr.Length - 1);
                    MessageBox.Show(showErrorsStr);
                    InBtn.IsEnabled = true;
                    LoginBtn.IsEnabled = true;
                    RegistrationBtn.IsEnabled = true;
                    return;
                }

                var requestUser = new User
                {
                    Login = login,
                    Password = password
                };

                SingletoneObj.Client = new TcpClient(SingletoneObj.Ip, SingletoneObj.Port);
                SingletoneObj.Stream = SingletoneObj.Client.GetStream();

                string request = tcpService.SerializeLoginRequest(requestUser);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                User user = tcpService.DeserializeLoginResponse(response);
                if (!user.Login.Equals(login) || !user.Password.Equals(password))
                    throw new ArgumentException("login or password is incorrect");

                SingletoneObj.User = user;
                SingletoneObj.Windows.Add("LoginWindow", this);
                MainWindow window = new MainWindow();
                window.Show();
            }
            catch(Exception ex)
            {
                InBtn.IsEnabled = true;
                LoginBtn.IsEnabled = true;
                RegistrationBtn.IsEnabled = true;
                if (SingletoneObj.Client != null)
                    SingletoneObj.Client.Close();
                if (SingletoneObj.Stream != null)
                    SingletoneObj.Stream.Close();
                MessageBox.Show(ex.Message);
            }



        }

        private void Button_Reg_Click(object sender, RoutedEventArgs e)
        {
            SingletoneObj.Windows.Add("LoginWindow", this);
            RegisterWindow window = new RegisterWindow();
            window.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
