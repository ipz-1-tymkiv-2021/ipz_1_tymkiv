﻿using Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Services.Tests
{
    [TestClass()]
    public class TcpServiceTests
    {
       [TestMethod()]
        public void SerializeLoginRequestTest()
        {
            var tcpService = new TcpService(new ApplicationContext());
            string expended = "{\"Method\":\"Login\",\"User\":\"{\\u0022Id\\u0022:0,\\u0022Login\\u0022:\\u0022Alex\\u0022,\\u0022Password\\u0022:\\u0022123\\u0022,\\u0022Role_User_Mappings\\u0022:[]}\",\"Args\":null}";
            string actual = tcpService.SerializeLoginRequest(new User { Login = "Alex", Password = "123" });
            Assert.AreEqual(expended, actual);
        }

        [TestMethod()]
        public void SerializeRegisterRequestTest()
        {
            var tcpService = new TcpService(new ApplicationContext());
            string expended = "{\"Method\":\"Login\",\"User\":\"{\\u0022Id\\u0022:0,\\u0022Login\\u0022:\\u0022Alex\\u0022,\\u0022Password\\u0022:\\u0022123\\u0022,\\u0022Role_User_Mappings\\u0022:[]}\",\"Args\":null}";
            string actual = tcpService.SerializeLoginRequest(new User { Login = "Alex", Password = "123" });
            Assert.AreEqual(expended, actual);
        }

        [TestMethod()]
        public void SerializeInitServicesInServicesWindowTest()
        {
            var tcpService = new TcpService(new ApplicationContext());
            string expended = "{\"Method\":\"Login\",\"User\":\"{\\u0022Id\\u0022:0,\\u0022Login\\u0022:\\u0022Alex\\u0022,\\u0022Password\\u0022:\\u0022123\\u0022,\\u0022Role_User_Mappings\\u0022:[]}\",\"Args\":null}";
            string actual = tcpService.SerializeLoginRequest(new User { Login = "Alex", Password = "123" });
            Assert.AreEqual(expended, actual);
        }

        [TestMethod()]
        public void DeserializeInitServicesInServicesWindowTest()
        {
            var tcpService = new TcpService(new ApplicationContext());

            var expected = new List<Service> {
                new Service
                {
                    Id = 0, 
                    Master = null,
                    Name = "qwerty",
                    Price = 11.20M
                   
                }
            };

            string str = "[{\"Id\":0,\"Name\":\"qwerty\",\"Price\":11.20,\"Master\":null}]";

            var actual = tcpService.DeserializeInitServicesInServicesWindow(str);

            Assert.AreEqual(expected[0].Name, actual[0].Name);
            Assert.AreEqual(expected[0].Id, actual[0].Id);
            Assert.AreEqual(expected[0].Price, actual[0].Price);
            Assert.AreEqual(expected[0].Master, actual[0].Master);
        }

        [TestMethod()]
        public void SerializeInitMastersInMastersWindowTest()
        {
            var tcpService = new TcpService(new ApplicationContext());
            string expended = "{\"Method\":\"Login\",\"User\":\"{\\u0022Id\\u0022:0,\\u0022Login\\u0022:\\u0022Alex\\u0022,\\u0022Password\\u0022:\\u0022123\\u0022,\\u0022Role_User_Mappings\\u0022:[]}\",\"Args\":null}";
            string actual = tcpService.SerializeLoginRequest(new User { Login = "Alex", Password = "123" });
            Assert.AreEqual(expended, actual);
        }

        [TestMethod()]
        public void DeserializeInitMastersInMastersWindowTest()
        {
            var tcpService = new TcpService(new ApplicationContext());

            var expected = new List<Master> {
                new Master
                {
                    Id = 0,
                    Name = "qwerty",
                    Service = null
                }
            };

            string str = "[{\"Id\":0,\"Name\":\"qwerty\",\"Service\":null}]";

            var actual = tcpService.DeserializeInitMastersInMastersWindow(str);

            Assert.AreEqual(expected[0].Id, actual[0].Id);
            Assert.AreEqual(expected[0].Name, actual[0].Name);
            Assert.AreEqual(expected[0].Service, actual[0].Service);
        }

        [TestMethod()]
        public void SerializeAddMasterRequestTest()
        {
            var tcpService = new TcpService(new ApplicationContext());
            string expended = "{\"Method\":\"AddMaster\",\"User\":\"{\\u0022Id\\u0022:0,\\u0022Login\\u0022:\\u0022Alex\\u0022,\\u0022Password\\u0022:\\u0022123\\u0022,\\u0022Role_User_Mappings\\u0022:[]}\",\"Args\":\"{\\u0022Id\\u0022:0,\\u0022Name\\u0022:\\u0022qwerty\\u0022,\\u0022Service\\u0022:null,\\u0022Info\\u0022:\\u0022Name: qwerty\\\\tService: \\\\tPrice: 0\\u0022}\"}";
            var master = new Master
            {
                Id = 0,
                Name = "qwerty",
                Service = null
            };

            string actual = tcpService.SerializeAddMasterRequest(master,new User { Login = "Alex", Password = "123" });
            Assert.AreEqual(expended, actual);
        }

        [TestMethod()]
        public void SerializeDeleteMasterTest()
        {
            var tcpService = new TcpService(new ApplicationContext());
            string expended = "{\"Method\":\"DeleteMaster\",\"User\":\"{\\u0022Id\\u0022:0,\\u0022Login\\u0022:\\u0022Alex\\u0022,\\u0022Password\\u0022:\\u0022123\\u0022,\\u0022Role_User_Mappings\\u0022:[]}\",\"Args\":\"1\"}";
            string actual = tcpService.SerializeDeleteMaster(1,new User { Login = "Alex", Password = "123" });
            Assert.AreEqual(expended, actual);
        }
    }
}