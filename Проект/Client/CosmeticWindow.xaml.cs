﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for CosmeticWindow.xaml
    /// </summary>
    public partial class CosmeticWindow : Window
    {
        private readonly TcpService tcpService;

        public static async Task<CosmeticWindow> CreateAsyncCosmeticsWindow()
        {
            CosmeticWindow window = new CosmeticWindow();
            await window.InitializeAsync();
            return window;
        }

        private CosmeticWindow()
        {
            tcpService = new TcpService();
        }

        private async Task InitializeAsync()
        {
            try
            {
                InitializeComponent();
                if (!SingletoneObj.User.Role_User_Mappings.Any(el => el.Role.RoleName.Equals("Admin")))
                {
                    AddBtn.Visibility = Visibility.Hidden;
                    DeleteBtn.Visibility = Visibility.Hidden;
                }
                var make = await InitModelsInWindow();
                SingletoneObj.makeups.Clear();
                foreach (var el in make)
                {
                    SingletoneObj.makeups.Add(el);
                }
                MakeupsListBox.ItemsSource = SingletoneObj.makeups;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<List<Makeup>> InitModelsInWindow()
        {
            try
            {
                string request = tcpService.SerializeInitMakeupsInMskeupsWindow(SingletoneObj.User);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<Makeup> models = tcpService.DeserializeInitMakeupsInMakeupsWindow(response);
                return models;
            }
            catch (Exception)
            {
                return new List<Makeup>();
            }
        }

        private async void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DeleteBtn.IsEnabled = false;
                AddBtn.IsEnabled = false;
                ExitBtn.IsEnabled = false;

                object makeup = MakeupsListBox.SelectedItem;
                int? id = ((Makeup)makeup)?.Id;
                if (!id.HasValue)
                    throw new ArgumentException($"{nameof(id)} is incorrect");

                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.No)
                {
                    DeleteBtn.IsEnabled = true;
                    AddBtn.IsEnabled = true;
                    ExitBtn.IsEnabled = true;
                    return;
                }

                string request = tcpService.SerializeDeleteMakeup(id.Value, SingletoneObj.User);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);

                var responseArgs = response.Split(';');
                if (responseArgs.Length > 1 && responseArgs[0].Contains("500"))
                {
                    throw new ArgumentException(responseArgs[1]);
                }
                if (responseArgs.Length == 1 && responseArgs[0].Equals("200"))
                {
                    SingletoneObj.makeups.Remove((Makeup)makeup);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                DeleteBtn.IsEnabled = true;
                AddBtn.IsEnabled = true;
                ExitBtn.IsEnabled = true;
            }
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            AddMakeup window = new AddMakeup();
            window.ShowDialog();
        }
    }
}
