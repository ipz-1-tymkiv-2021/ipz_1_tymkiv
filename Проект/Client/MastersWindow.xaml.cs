﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MastersWindow.xaml
    /// </summary>
    public partial class MastersWindow : Window
    {
        private readonly TcpService tcpService;

        public static async Task<MastersWindow> CreateAsyncMastersWindow()
        {
            MastersWindow window = new MastersWindow();
            await window.InitializeAsync();
            return window;
        }

        private MastersWindow()
        {
            tcpService = new TcpService();
        }

        private async Task InitializeAsync()
        {
            try
            {
                InitializeComponent();
                if(!SingletoneObj.User.Role_User_Mappings.Any(el => el.Role.RoleName.Equals("Admin")))
                {
                    AddBtn.Visibility = Visibility.Hidden;
                    DeleteBtn.Visibility = Visibility.Hidden;
                }
                var mast = await InitModelsInWindow();
                SingletoneObj.masters.Clear();
                foreach (var el in mast)
                {
                    SingletoneObj.masters.Add(el);
                }
                MastersListBox.ItemsSource = SingletoneObj.masters;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<List<Master>> InitModelsInWindow()
        {
            try
            {
                string request = tcpService.SerializeInitMastersInMastersWindow(SingletoneObj.User);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<Master> models = tcpService.DeserializeInitMastersInMastersWindow(response);
                return models;
            }
            catch (Exception)
            {
                return new List<Master>();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            AddMaster window = new AddMaster();
            window.ShowDialog();
        }

        private async void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DeleteBtn.IsEnabled = false;
                AddBtn.IsEnabled = false;
                ExitBtn.IsEnabled = false;

                object master = MastersListBox.SelectedItem;
                int? id = ((Master)master)?.Id;
                if (!id.HasValue)
                    throw new ArgumentException($"{nameof(id)} is incorrect");

                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.No)
                {
                    DeleteBtn.IsEnabled = true;
                    AddBtn.IsEnabled = true;
                    ExitBtn.IsEnabled = true;
                    return;
                }

                string request = tcpService.SerializeDeleteMaster(id.Value, SingletoneObj.User);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);

                var responseArgs = response.Split(';');
                if (responseArgs.Length > 1 && responseArgs[0].Contains("500"))
                {
                    throw new ArgumentException(responseArgs[1]);
                }
                if (responseArgs.Length == 1 && responseArgs[0].Equals("200"))
                {
                    SingletoneObj.masters.Remove((Master)master);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( ex.Message);
            }
            finally
            {
                DeleteBtn.IsEnabled = true;
                AddBtn.IsEnabled = true;
                ExitBtn.IsEnabled = true;
            }
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
