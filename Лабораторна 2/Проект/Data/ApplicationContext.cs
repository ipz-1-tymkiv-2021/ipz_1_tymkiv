﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Role_User_Mapping> Role_User_Mappings {get;set;}
        public DbSet<Master> Masters { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Makeup> Makeups { get; set; }
        public ApplicationContext() : base(@"server=WIN-4KA2NN73G0M\SQLEXPRESS;database=BeautySalonDB;trusted_connection=true;") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //User
            modelBuilder.Entity<User>()
                .ToTable("Users");
            modelBuilder.Entity<User>()
                .HasKey(a => a.Id);
            modelBuilder.Entity<User>()
                .Property(a => a.Login).IsRequired();
            modelBuilder.Entity<User>()
                .Property(a => a.Password).IsRequired();

            //Roles
            modelBuilder.Entity<Role>()
                .ToTable("Roles");
            modelBuilder.Entity<Role>()
                .HasKey(a => a.Id);
            modelBuilder.Entity<Role>()
                .Property(a => a.RoleName).IsRequired();

            //Role_User_Mapping
            modelBuilder.Entity<Role_User_Mapping>()
                .ToTable("Role_User_Mappings");
            modelBuilder.Entity<Role_User_Mapping>()
                .HasKey(a => a.Id);
            modelBuilder.Entity<Role_User_Mapping>()
                .HasRequired(a => a.Role)
                .WithMany(a => a.Role_User_Mappings)
                .HasForeignKey(a => a.RoleId)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Role_User_Mapping>()
                .HasRequired(a => a.User)
                .WithMany(a => a.Role_User_Mappings)
                .HasForeignKey(a => a.UserId)
                .WillCascadeOnDelete(true);

            //Master
            modelBuilder.Entity<Master>()
                .ToTable("Masters");
            modelBuilder.Entity<Master>()
                .HasKey(a => a.Id);
            modelBuilder.Entity<Master>()
                .Property(a => a.Name).IsRequired();
            modelBuilder.Entity<Master>()
                .HasRequired(a => a.Service)
                .WithRequiredPrincipal(a => a.Master);

            //Service
            modelBuilder.Entity<Service>()
                .ToTable("Serivices");
            modelBuilder.Entity<Service>()
                .HasKey(a => a.Id);
            modelBuilder.Entity<Service>()
                .Property(a => a.Name).IsRequired();
            modelBuilder.Entity<Service>()
                .Property(a => a.Price).IsRequired().HasPrecision(18, 2);
            modelBuilder.Entity<Service>()
                .HasRequired(a => a.Master)
                .WithRequiredDependent(a => a.Service)
                .WillCascadeOnDelete(true);

            //Makeup
            modelBuilder.Entity<Makeup>()
                .ToTable("Makeups");
            modelBuilder.Entity<Makeup>()
                .HasKey(a => a.Id);
            modelBuilder.Entity<Makeup>()
                .Property(a => a.Name).IsRequired();
            modelBuilder.Entity<Makeup>()
                .Property(a => a.Price).IsRequired().HasPrecision(18, 2);
        }
    }
}
