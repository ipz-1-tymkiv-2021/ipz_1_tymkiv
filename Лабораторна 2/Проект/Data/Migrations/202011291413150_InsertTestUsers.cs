﻿namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertTestUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO Roles (RoleName) VALUES ('User'), ('Admin')");
            Sql(@"INSERT INTO Users (Login, Password) VALUES ('Slavik','123'), ('NeSlavik','123')");
            Sql(@"INSERT INTO Role_User_Mappings (RoleId, UserId) VALUES (1,1), (2,1), (1,2)");
        }
        
        public override void Down()
        {
        }
    }
}
