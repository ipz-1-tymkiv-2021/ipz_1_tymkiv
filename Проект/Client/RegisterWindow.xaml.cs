﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        private readonly TcpService tcpService;
        public RegisterWindow()
        {
            InitializeComponent();
            if (SingletoneObj.Windows.ContainsKey("LoginWindow"))
            {
                SingletoneObj.Windows["LoginWindow"].Close();
                SingletoneObj.Windows.Remove("LoginWindow");
            }
            tcpService = new TcpService();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RegisterBtn.IsEnabled = false;
                RegBtn.IsEnabled = false;
                LogNtm.IsEnabled = false;

                List<string> errors = new List<string>();
                string login = textBoxLogin.Text;
                string password = passBox.Password;
                string confPassword = passBox_2.Password;
                if (string.IsNullOrWhiteSpace(login))
                    errors.Add($"'{nameof(login)} is incorrect'");
                if (string.IsNullOrWhiteSpace(password))
                    errors.Add($"'{nameof(password)}' is incorrect");
                if (string.IsNullOrWhiteSpace(password))
                    errors.Add($"'{nameof(confPassword)}' is incorrect");
                if (!password.Equals(confPassword))
                    errors.Add("passwords do not match");

                if (errors.Any())
                {
                    string showErrorsStr = "";
                    foreach (var error in errors)
                        showErrorsStr += error + "\n";
                    showErrorsStr = showErrorsStr.Remove(showErrorsStr.Length - 1);
                    MessageBox.Show(showErrorsStr);
                    RegisterBtn.IsEnabled = true;
                    RegBtn.IsEnabled = true;
                    LogNtm.IsEnabled = true;
                    return;
                }

                var requestUser = new User
                {
                    Login = login,
                    Password = password
                };

                SingletoneObj.Client = new TcpClient(SingletoneObj.Ip, SingletoneObj.Port);
                SingletoneObj.Stream = SingletoneObj.Client.GetStream();

                string request = tcpService.SerializeRegisterRequest(requestUser);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                User user = tcpService.DeserializeRegisterResponse(response);
                if (!user.Login.Equals(login) || !user.Password.Equals(password))
                    throw new ArgumentException("login or password is incorrect");

                SingletoneObj.User = user;
                SingletoneObj.Windows.Add("RegisterWindow", this);
                MainWindow window = new MainWindow();
                window.Show();
            }
            catch (Exception ex)
            {
                RegisterBtn.IsEnabled = true;
                RegBtn.IsEnabled = true;
                LogNtm.IsEnabled = true;
                if (SingletoneObj.Client != null)
                    SingletoneObj.Client.Close();
                if (SingletoneObj.Stream != null)
                    SingletoneObj.Stream.Close();
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SingletoneObj.Windows.Add("RegisterWindow", this);
            LoginWindow window = new LoginWindow();
            window.Show();
        }
    }
}
