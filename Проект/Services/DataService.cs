﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DataService
    {
        private readonly ApplicationContext _dbContext;
        public DataService(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public User GetUser(User user)
        {
            return _dbContext.Users.Where(el => el.Login.Equals(user.Login) && el.Password.Equals(user.Password))
                .Include(el => el.Role_User_Mappings.Select(x => x.Role))
                .FirstOrDefault();
        }

        public User RegisterUser(User user)
        {
            var userInDb = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login));
            var role = _dbContext.Roles.FirstOrDefault(el => el.RoleName.Equals("User"));
            if(userInDb!= null || role == null)
            {
                throw new ArgumentException();
            }
            var query = @"INSERT INTO Users (Login, Password) VALUES (@Login, @Password)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Login", user.Login), new SqlParameter("@Password", user.Password));
            var userId = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login)).Id;
            query = @"INSERT INTO Role_User_Mappings (RoleId, UserId) VALUES (@RoleId, @UserId)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@RoleId", role.Id), new SqlParameter("@UserId", userId));

            return _dbContext.Users.Where(el => el.Login.Equals(user.Login) && el.Password.Equals(user.Password))
               .Include(el => el.Role_User_Mappings.Select(x => x.Role))
               .FirstOrDefault();
        }

        public List<Service> GetAllServices()
        {
            return _dbContext.Services.ToList();
        }

        public List<Master> GetAllMasters()
        {
            var masters =  _dbContext.Masters.ToList();
            foreach(var el in masters)
            {
                el.Service = _dbContext.Services.FirstOrDefault(x => x.Id == el.Id);
            }
            return masters;
        }

        public int InsertMaster(Master master)
        {
            var masterInDb = _dbContext.Masters.FirstOrDefault(el => el.Name.Equals(master.Name));
            if (masterInDb != null)
                throw new ArgumentException("The master does not exist");
            var query = "INSERT INTO Masters (Name) VALUES (@Name)";
            _dbContext.Database.ExecuteSqlCommand(query,
                new SqlParameter("@Name", master.Name));
            var ms = _dbContext.Masters.FirstOrDefault(el => el.Name.Equals(master.Name));
            if(ms == null)
            {
                throw new ArgumentException("The master does not exist");
            }
            query = "INSERT INTO Serivices (Id, Name, Price) VALUES (@Id, @Name, @Price)";
            _dbContext.Database.ExecuteSqlCommand(query,
                new SqlParameter("@Id", ms.Id),
                new SqlParameter("@Name", master.Service.Name),
                new SqlParameter("@Price", master.Service.Price));
            return ms.Id;
        }

        public void DeleteMaster(int id)
        {
            var service = _dbContext.Services.FirstOrDefault(el => el.Id == id);
            if (service == null)
                throw new ArgumentException("The service does not exist");
            var query = "DELETE FROM [dbo].[Serivices] WHERE Id = @Id";
            _dbContext.Database.ExecuteSqlCommand(query,
                new SqlParameter("@Id", id));
            var master = _dbContext.Masters.FirstOrDefault(el => el.Id == id);
            if (master == null)
                throw new ArgumentException("The master does not exist");
            query = "DELETE FROM Masters WHERE Id = @Id";
            _dbContext.Database.ExecuteSqlCommand(query,
                new SqlParameter("@Id", id));
        }

        public List<Makeup> GetAllMakeups()
        {
            return _dbContext.Makeups.ToList();
        }

        public int InsertMakeup(Makeup makeup)
        {
            var makeupInDb = _dbContext.Makeups.FirstOrDefault(el => el.Name.Equals(makeup.Name));
            if(makeupInDb != null)
                throw new ArgumentException("The makeup does not exist");
            var query = "INSERT INTO [dbo].[Makeups] (Name, Price) VALUES (@Name, @Price)";
            _dbContext.Database.ExecuteSqlCommand(query,
                new SqlParameter("@Name", makeup.Name),
                new SqlParameter("@Price", makeup.Price));
            makeupInDb = _dbContext.Makeups.FirstOrDefault(el => el.Name.Equals(makeup.Name));
            if (makeupInDb == null)
                throw new ArgumentException("The makeup does not exist");
            return makeupInDb.Id;
        }

        public void DeleteMakeup(int id)
        {
            var makeup = _dbContext.Makeups.FirstOrDefault(el => el.Id == id);
            if (makeup == null)
                throw new ArgumentException("The makeup does not exist");
            var query = "DELETE FROM Makeups WHERE Id = @Id";
            _dbContext.Database.ExecuteSqlCommand(query,
                new SqlParameter("@Id", id));
        }

    }
}
