﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Services
{
    public class TcpService
    {
        private readonly DataService dataService;
        public TcpService(ApplicationContext context = null)
        {
            if (context != null)
                dataService = new DataService(context);
        }


        public string DecodeAndProcessRequest(string request)
        {
            var socketNode = JsonSerializer.Deserialize<SocketNode>(request);
            TcpMethods tcpMethod;
            string response = "";
            if (!Enum.TryParse<TcpMethods>(socketNode.Method, out tcpMethod))
            {
                tcpMethod = TcpMethods.NONE;
            }


            switch (tcpMethod)
            {
                case TcpMethods.Login:
                    response = Login(socketNode);
                    break;
                case TcpMethods.Register:
                    response = Register(socketNode);
                    break;
                case TcpMethods.InitServicesWindow:
                    response = InitServicesWindow();
                    break;
                case TcpMethods.InitMastersWindow:
                    response = InitMastersWindow();
                    break;
                case TcpMethods.AddMaster:
                    response = AddMaster(socketNode);
                    break;
                case TcpMethods.DeleteMaster:
                    response = DeleteMaster(socketNode);
                    break;
                case TcpMethods.InitMakeupsWindow:
                    response = InitMakeupsWindow();
                    break;
                case TcpMethods.AddMakeup:
                    response = AddMakeup(socketNode);
                    break;
                case TcpMethods.DeleteMakeup:
                    response = DeleteMakeup(socketNode);
                    break;
                default:
                    break;
            }

            return response;
        }

        public async Task<byte[]> CodeStreamAsync(string request)
        {
            return await Task.Run(() => CodeStream(request));
        }

        public byte[] CodeStream(string request)
        {
            return Encoding.UTF8.GetBytes(request);
        }

        public async Task<string> DecodeStreamAsync(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = await stream.ReadAsync(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public string DecodeStream(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }


        public string SerializeLoginRequest(User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Login",
                User = JsonSerializer.Serialize<User>(user)
            });
        }

        public User DeserializeLoginResponse(string response)
        {
            return JsonSerializer.Deserialize<User>(response);
        }

        public string Login(SocketNode socketNode)
        {
            try
            {
                var user = dataService.GetUser(JsonSerializer.Deserialize<User>(socketNode.User));
                foreach (var map in user.Role_User_Mappings)
                {
                    map.Role.Role_User_Mappings = null;
                    map.User = null;
                }
                return JsonSerializer.Serialize<User>(user);
            }
            catch (Exception)
            {
                return JsonSerializer.Serialize<User>(new User { Password = "", Login = "" });
            }
        }

        public string SerializeRegisterRequest(User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Register",
                User = JsonSerializer.Serialize<User>(user)
            });
        }

        public User DeserializeRegisterResponse(string response)
        {
            return JsonSerializer.Deserialize<User>(response);
        }

        public string Register(SocketNode socketNode)
        {
            try
            {
                var user = dataService.RegisterUser(JsonSerializer.Deserialize<User>(socketNode.User));
                foreach (var map in user.Role_User_Mappings)
                {
                    map.Role.Role_User_Mappings = null;
                    map.User = null;
                }
                return JsonSerializer.Serialize<User>(user);
            }
            catch (Exception)
            {
                return JsonSerializer.Serialize<User>(new User { Password = "", Login = "" });
            }
        }

        public string SerializeInitServicesInServicesWindow(User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode {
                Method = "InitServicesWindow",
                User = JsonSerializer.Serialize<User>(user)
            });
        }

        public List<Service> DeserializeInitServicesInServicesWindow(string response)
        {
            return JsonSerializer.Deserialize<List<Service>>(response);
        }

        private string InitServicesWindow()
        {
            try
            {
                var services = dataService.GetAllServices();
                foreach (var el in services)
                {
                    el.Master = null;
                }
                return JsonSerializer.Serialize<List<Service>>(services);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SerializeInitMastersInMastersWindow(User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "InitMastersWindow",
                User = JsonSerializer.Serialize<User>(user)
            });
        }

        public List<Master> DeserializeInitMastersInMastersWindow(string response)
        {
            return JsonSerializer.Deserialize<List<Master>>(response);
        }

        private string InitMastersWindow()
        {
            try
            {
                var masters = dataService.GetAllMasters();
                foreach(var el in masters)
                {
                    if(el.Service!=null)
                        el.Service.Master = null;
                }
                return JsonSerializer.Serialize<List<Master>>(masters);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SerializeAddMasterRequest(Master master, User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode {
                Method = "AddMaster",
                User = JsonSerializer.Serialize<User>(user),
                Args = JsonSerializer.Serialize<Master>(master)
            });
        }

        private string AddMaster(SocketNode socketNode)
        {
            try
            {
                var requestMaster = JsonSerializer.Deserialize<Master>(socketNode.Args);
                int id = dataService.InsertMaster(requestMaster);
                return "200;" + id.ToString();
            }
            catch (Exception ex)
            {
                return "500;" + ex.Message;
            }
        }

        public string SerializeDeleteMaster(int id, User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode {
                Method = "DeleteMaster",
                User = JsonSerializer.Serialize<User>(user),
                Args = id.ToString()
            });
        }

        private string DeleteMaster(SocketNode socketNode)
        {
            try
            {
                int id = JsonSerializer.Deserialize<int>(socketNode.Args);
                dataService.DeleteMaster(id);
                return "200";
            }
            catch (Exception ex)
            {
                return "500;" + ex.Message;
            }

        }

        public string SerializeInitMakeupsInMskeupsWindow(User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "InitMakeupsWindow",
                User = JsonSerializer.Serialize<User>(user)
            });
        }

        public List<Makeup> DeserializeInitMakeupsInMakeupsWindow(string response)
        {
            return JsonSerializer.Deserialize<List<Makeup>>(response);
        }

        private string InitMakeupsWindow()
        {
            try
            {
                var makeups = dataService.GetAllMakeups();
                return JsonSerializer.Serialize<List<Makeup>>(makeups);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SerializeAddMakeupRequest(Makeup makeup, User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "AddMakeup",
                User = JsonSerializer.Serialize<User>(user),
                Args = JsonSerializer.Serialize<Makeup>(makeup)
            });
        }

        private string AddMakeup(SocketNode socketNode)
        {
            try
            {
                var requestMakeup = JsonSerializer.Deserialize<Makeup>(socketNode.Args);
                int id = dataService.InsertMakeup(requestMakeup);
                return "200;" + id.ToString();
            }
            catch (Exception ex)
            {
                return "500;" + ex.Message;
            }
        }

        public string SerializeDeleteMakeup(int id, User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "DeleteMakeup",
                User = JsonSerializer.Serialize<User>(user),
                Args = id.ToString()
            });
        }

        private string DeleteMakeup(SocketNode socketNode)
        {
            try
            {
                int id = JsonSerializer.Deserialize<int>(socketNode.Args);
                dataService.DeleteMakeup(id);
                return "200";
            }
            catch (Exception ex)
            {
                return "500;" + ex.Message;
            }
        }

    }




    public enum TcpMethods
    {
        NONE,
        Login,
        Register,
        InitServicesWindow,
        InitMastersWindow,
        AddMaster,
        DeleteMaster,
        InitMakeupsWindow,
        AddMakeup,
        DeleteMakeup
    }
}
