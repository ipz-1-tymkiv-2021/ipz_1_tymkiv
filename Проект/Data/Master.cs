﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{  
    public class Master
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Service Service { get; set; }

        [NotMapped]
        public string Info
        {
            get { return $"Name: {Name}\tService: {Service?.Name ?? ""}\tPrice: {Service?.Price ?? 0}"; }
        }

    }
}
