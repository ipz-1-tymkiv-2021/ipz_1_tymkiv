﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for ServicesWindow.xaml
    /// </summary>
    public partial class ServicesWindow : Window
    {
        private readonly TcpService tcpService;
        private ObservableCollection<Service> services = new ObservableCollection<Service>();

        public static async Task<ServicesWindow> CreateAsyncServicesWindow()
        {
            ServicesWindow window = new ServicesWindow();
            await window.InitializeAsync();
            return window;
        }

        private ServicesWindow()
        {
            tcpService = new TcpService();
        }

        private async Task InitializeAsync()
        {
            try
            {
                InitializeComponent();
                var serv = await InitModelsInWindow();
                foreach(var el in serv)
                {
                    services.Add(el);
                }
                ServicesListBox.ItemsSource = services;
            }
            catch(Exception)
            {
                throw;
            }
        }

        private async Task<List<Service>> InitModelsInWindow()
        {
            try
            {
                string request = tcpService.SerializeInitServicesInServicesWindow(SingletoneObj.User);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<Service> models = tcpService.DeserializeInitServicesInServicesWindow(response);
                return models;
            }
            catch (Exception)
            {
                return new List<Service>();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
