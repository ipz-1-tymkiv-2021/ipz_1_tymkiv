﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for AddMaster.xaml
    /// </summary>
    public partial class AddMaster : Window
    {
        private readonly TcpService tcpService;
        public AddMaster()
        {
            InitializeComponent();
            tcpService = new TcpService();
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddBtn.IsEnabled = false;
                ExitBtn.IsEnabled = false;

                string name = textBoxMasterName.Text;
                string serviceName = textBoxServiceName.Text;
                decimal price;

                List<string> errors = new List<string>();
                if (string.IsNullOrWhiteSpace(name))
                    errors.Add("name is incorrect");
                if(string.IsNullOrWhiteSpace(serviceName))
                    errors.Add("service name is incorrect");
                if (!decimal.TryParse(textBoxServicePrice.Text, out price))
                    errors.Add("price is incorrect");

                if (errors.Any())
                {
                    string errorStr = "";
                    foreach (var el in errors)
                        errorStr += el + "\n";
                    AddBtn.IsEnabled = true;
                    ExitBtn.IsEnabled = true;
                    MessageBox.Show(errorStr);
                    return;
                }

                var requestMaster = new Master
                {
                    Name = name,
                    Service = new Service
                    {
                        Name = serviceName,
                        Price = price
                    }
                };

                string request = tcpService.SerializeAddMasterRequest(requestMaster, SingletoneObj.User);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                var responseArgs = response.Split(';');
                if (responseArgs.Length > 1 && responseArgs[0].Contains("500"))
                {
                    throw new ArgumentException(responseArgs[1]);
                }
                if (responseArgs.Length > 1 && responseArgs[0].Contains("200"))
                {
                    requestMaster.Id = int.Parse(responseArgs[1]);
                }
                SingletoneObj.masters.Add(requestMaster);
                this.Close();
            }
            catch (Exception ex)
            {
                AddBtn.IsEnabled = true;
                ExitBtn.IsEnabled = true;
                MessageBox.Show(ex.Message);
            }
        }
    }
}
