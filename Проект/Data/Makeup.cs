﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Makeup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        [NotMapped]
        public string Info
        {
            get { return $"Name: {Name}\tPrice: {Price}"; }
        }
    }
}
