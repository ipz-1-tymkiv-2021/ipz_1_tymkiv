﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set;}
        public string Password { get; set; }

        private ICollection<Role_User_Mapping> _role_user_mappings;
        public ICollection<Role_User_Mapping> Role_User_Mappings
        {
            get { return _role_user_mappings ?? (_role_user_mappings = new List<Role_User_Mapping>()); }
            set { _role_user_mappings = value; }
        }
    }
}
