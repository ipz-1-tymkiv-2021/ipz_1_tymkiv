﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for AddMakeup.xaml
    /// </summary>
    public partial class AddMakeup : Window
    {
        private readonly TcpService tcpService;
        public AddMakeup()
        {
            InitializeComponent();
            tcpService = new TcpService();
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddBtn.IsEnabled = false;
                ExitBtn.IsEnabled = false;

                string name = textBoxName.Text;
                decimal price;

                List<string> errors = new List<string>();
                if (string.IsNullOrWhiteSpace(name))
                    errors.Add("name is incorrect");
                if (!decimal.TryParse(textBoxPrice.Text, out price))
                    errors.Add("price is incorrect");

                if (errors.Any())
                {
                    string errorStr = "";
                    foreach (var el in errors)
                        errorStr += el + "\n";
                    AddBtn.IsEnabled = true;
                    ExitBtn.IsEnabled = true;
                    MessageBox.Show(errorStr);
                    return;
                }

                var requestMakeup = new Makeup
                {
                    Name = name,
                    Price = price
                };

                string request = tcpService.SerializeAddMakeupRequest(requestMakeup, SingletoneObj.User);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                var responseArgs = response.Split(';');
                if (responseArgs.Length > 1 && responseArgs[0].Contains("500"))
                {
                    throw new ArgumentException(responseArgs[1]);
                }
                if (responseArgs.Length > 1 && responseArgs[0].Contains("200"))
                {
                    requestMakeup.Id = int.Parse(responseArgs[1]);
                }
                SingletoneObj.makeups.Add(requestMakeup);
                this.Close();
            }
            catch (Exception ex)
            {
                AddBtn.IsEnabled = true;
                ExitBtn.IsEnabled = true;
                MessageBox.Show(ex.Message);
            }
        }
    }
}
