﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Services
{
    public class TcpService
    {
        private readonly DataService dataService;
        public TcpService(ApplicationContext context = null)
        {
            if (context != null)
                dataService = new DataService(context);
        }


        public string DecodeAndProcessRequest(string request)
        {
            var socketNode = JsonSerializer.Deserialize<SocketNode>(request);
            TcpMethods tcpMethod;
            string response = "";
            if (!Enum.TryParse<TcpMethods>(socketNode.Method, out tcpMethod))
            {
                tcpMethod = TcpMethods.NONE;
            }


            switch (tcpMethod)
            {
                case TcpMethods.Login:
                    response = Login(socketNode);
                    break;
                case TcpMethods.Register:
                    response = Register(socketNode);
                    break;
                default:
                    break;
            }

            return response;
        }

        public async Task<byte[]> CodeStreamAsync(string request)
        {
            return await Task.Run(() => CodeStream(request));
        }

        public byte[] CodeStream(string request)
        {
            return Encoding.UTF8.GetBytes(request);
        }

        public async Task<string> DecodeStreamAsync(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = await stream.ReadAsync(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public string DecodeStream(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }


        public string SerializeLoginRequest(User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Login",
                User = JsonSerializer.Serialize<User>(user)
            });
        }

        public User DeserializeLoginResponse(string response)
        {
            return JsonSerializer.Deserialize<User>(response);
        }

        public string Login(SocketNode socketNode)
        {
            try
            {
                var user = dataService.GetUser(JsonSerializer.Deserialize<User>(socketNode.User));
                foreach(var map in user.Role_User_Mappings)
                {
                    map.Role.Role_User_Mappings = null;
                    map.User = null;
                }
                return JsonSerializer.Serialize<User>(user);
            }
            catch(Exception)
            {
                return JsonSerializer.Serialize<User>(new User { Password = "", Login = "" });
            }
        }

        public string SerializeRegisterRequest(User user)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Register",
                User = JsonSerializer.Serialize<User>(user)
            });
        }

        public User DeserializeRegisterResponse(string response)
        {
            return JsonSerializer.Deserialize<User>(response);
        }

        public string Register(SocketNode socketNode)
        {
            try
            {
                var user = dataService.RegisterUser(JsonSerializer.Deserialize<User>(socketNode.User));
                foreach (var map in user.Role_User_Mappings)
                {
                    map.Role.Role_User_Mappings = null;
                    map.User = null;
                }
                return JsonSerializer.Serialize<User>(user);
            }
            catch (Exception)
            {
                return JsonSerializer.Serialize<User>(new User { Password = "", Login = ""});
            }
        }

    }



    public enum TcpMethods
    {
        NONE,
        Login,
        Register
    }
}
