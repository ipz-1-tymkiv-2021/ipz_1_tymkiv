﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }   
        public decimal Price { get; set; }
        public Master Master { get; set; }

        [NotMapped]
        public string Info
        {
            get { return $"Id: {Id}\tName: {Name}\tPrice: {Price}"; }
        }
    }
}
