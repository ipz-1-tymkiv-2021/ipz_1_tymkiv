﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for UserMainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly TcpService tcpService;   
        public MainWindow()
        {
            InitializeComponent();
            if(SingletoneObj.Windows.ContainsKey("LoginWindow"))
            {
                SingletoneObj.Windows["LoginWindow"].Close();
                SingletoneObj.Windows.Remove("LoginWindow");
            }
            if (SingletoneObj.Windows.ContainsKey("RegisterWindow"))
            {
                SingletoneObj.Windows["RegisterWindow"].Close();
                SingletoneObj.Windows.Remove("RegisterWindow");
            }
            tcpService = new TcpService();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MastersBtn.IsEnabled = false;
                MastersBtn.IsEnabled = false;
                ServicesBtn.IsEnabled = false;
                ExitBtn.IsEnabled = false;

                string request = "LogOut";
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
            }
            catch (Exception) { }
            finally
            {
                if (SingletoneObj.Stream != null)
                    SingletoneObj.Stream.Close();
                if (SingletoneObj.Client != null)
                    SingletoneObj.Client.Close();
                SingletoneObj.Windows.Add("MainWindow", this);
                SingletoneObj.User = null;
                LoginWindow form = new LoginWindow();
                form.Show();
            }
        }

        private async void ServicesBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ServicesWindow window = await ServicesWindow.CreateAsyncServicesWindow();
                window.ShowDialog();
            }
            catch (Exception) { }
        }

        private async void MastersBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MastersWindow window = await MastersWindow.CreateAsyncMastersWindow();
                window.ShowDialog();
            }
            catch (Exception) { }
        }

        private async void MakeupsBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CosmeticWindow window = await CosmeticWindow.CreateAsyncCosmeticsWindow();
                window.ShowDialog();
            }
            catch (Exception) { }
        }
    }
}
